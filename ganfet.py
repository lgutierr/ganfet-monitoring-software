import os
import time
from collections import OrderedDict
import matplotlib.pyplot as plt
import matplotlib.style as style
import numpy as np
import atlas_mpl_style as ampl

class ganfet():
  ''' Single GaN FET class
  '''
  def __init__(self, fetID, boardID=1):
    self.__id = fetID
    self.__board = boardID
    self.__radDose = 0.027562 # See dosimetry document. Lower limit is 27562.0rad/hr and upper limit is 29385.2rad/hr
    self.__beamStops = [(1655216400,1655239080, 1655239080-1655216400)] # Beam interruptions: starting time, endtime, totalStoppedTime
    self.__data = OrderedDict()
    self.__dataDuringIrrad = OrderedDict()
    self.__databreaks = []
    self.__databreaksDuringIrrad = []
    self.__invalidData = [(1654905300,1655216400)]
    self.__invalidDataDuringIrrad = []
  
  def id(self):
    ''' Return GaN FET ID
    '''
    return self.__id

  def boardID(self):
    ''' Return the GaN FET board number
    '''
    return self.__board

  def totalRad(self, hours):
    ''' Returns the total radiation received by the GaNFETs
    '''
    return hours * self.__radDose

  def totalIrradTime(self, rad):
    ''' Returns the total irradiation time
    '''
    return rad / self.__radDose

  def addData(self, data):
    ''' data is a dataFrame
    '''
    # Get last point from previous run and first point from new run
    previousRunLastPoint = None
    newRunFirstPoint = None
    timeWithoutData = 0
    if len(self.__data) > 0:
      previousRunLastPoint = list(self.__data.items())[-1]
      for index, row in data.iterrows():
        newRunFirstPoint = (row[0], row[1])
        break
      timeWithoutData = newRunFirstPoint[0] - previousRunLastPoint[0]
    # Fill data when there is no data collection between runs just to have uniform plots
    pointsToAdd = []
    if timeWithoutData > 900:
      self.__databreaks.append((previousRunLastPoint[0], newRunFirstPoint[0]))
      numNewPoints = int(timeWithoutData/600.0)
      for i in range(numNewPoints):
        pointsToAdd.append((previousRunLastPoint[0]+600*(i+1), previousRunLastPoint[1]))
    for point in pointsToAdd:
      self.addDataPoint(point)
    # Add new dataset
    for index, row in data.iterrows():
      self.addDataPoint((row['timestamp'], row['ganfet'+str((self.__id-1) % 20 + 1)]))
    self.__data = OrderedDict(sorted(self.__data.items(), key=lambda t: t[0])) # Sort orderedDict in terms of time

  def addDataPoint(self, point, sort=False):
    ''' First entry in point is time, second entry in point is voltage measurement
    '''
    self.__data[point[0]] = point[1]
    if sort: 
      self.__data = OrderedDict(sorted(self.__data.items(), key=lambda t: t[0])) # Sort orderedDict in terms of time
  
  def __beamStopShifts(self, typeData=True, inputSet=None, outputSet=None):
    ''' Shifts all necessary sets due to beam stops
    '''
    if typeData:
      for timestamp, voltage in self.__data.items():
        timeshift = 0.0
        delete = False
        for start, end, lapsed in self.__beamStops:
          if timestamp > start and timestamp < end:
            # Do not include data during beam stops
            delete = True
          elif timestamp > end:
            # If data after a beam stop, time needs to shift
            timeshift += lapsed
        if not delete:
          self.__dataDuringIrrad[str(timestamp-timeshift)] = voltage
    else:
      if inputSet != None and outputSet != None:
        for setEntryStartTime, setEntryEndTime in inputSet:
          startTimeShift = 0.0
          endTimeShift = 0.0
          delete = False
          for startStop, endStop, stopTotalTime in self.__beamStops:
            if setEntryStartTime > startStop and setEntryEndTime < endStop:
              # Delete entry that happened when there was no beam
              delete = True
            elif setEntryStartTime > endStop:
              # Databreak after a stop should be shifted by the beam stop time
              startTimeShift += stopTotalTime
              endTimeShift += stopTotalTime
            elif setEntryStartTime > startStop and setEntryStartTime < endStop and setEntryEndTime > endStop:
              # Start of entry during beam stop but end of entry after beam stop
              startTimeShift += stopTotalTime - (endStop - setEntryStartTime)
              endTimeShift += stopTotalTime
            elif setEntryEndTime < endStop and setEntryEndTime > startStop and setEntryStartTime < startStop:
              # End of databreak during beam stop but start of databreak before beam stop
              endTimeShift += setEntryEndTime - startStop
            elif setEntryEndTime > endStop and setEntryStartTime < startStop: 
              # Beam stop between databreak
              endTimeShift += stopTotalTime
            else:
              # Databreak before beam stop, nothing should happen
              pass
          if not delete:
            point1 = setEntryStartTime - startTimeShift
            point2 = setEntryEndTime - endTimeShift
            outputSet.append((point1, point2))

  def computeDataDuringIrrad(self):
    ''' Compute monitored voltage as a function of total irradiation time
    '''
    # Irradiation starting timestamp
    startingTimestamp = list(self.__data.items())[0][0]
    # Compute data
    self.__beamStopShifts(True)
    # Compute databreaks
    self.__beamStopShifts(False, self.__databreaks, self.__databreaksDuringIrrad)
    # Compute invalid data
    self.__beamStopShifts(False, self.__invalidData, self.__invalidDataDuringIrrad)
  
  def dataDuringIrradXY(self, hours=False):
    ''' Return a list of two lists. First element corresponds to timestamps(X) and second element corresponds to measured voltages(Y)
    '''
    # Configure data
    if hours:
      times = [*self.__dataDuringIrrad.keys()]
      x = [(float(i)-float(times[0]))/3600.0 for i in times]
      breaks = [( (float(i[0])-float(times[0]))/3600.0, (float(i[1])-float(times[0]))/3600.0 ) for i in self.__databreaksDuringIrrad]
      invalids = [( (float(i[0])-float(times[0]))/3600.0, (float(i[1])-float(times[0]))/3600.0 ) for i in self.__invalidDataDuringIrrad]
    else:
      x = list(self.__dataDuringIrrad.keys())
      breaks = self.__databreaksDuringIrrad
      invalids = self.__invalidDataDuringIrrad
    y = list(self.__dataDuringIrrad.values())
    return [x, y, breaks, invalids]

  def printAllData(self):
    ''' Print data
    '''
    print("Data points: "+str(len(self.__data)))
    for key in self.__data:
      print("Timestamp: " + str(key) + " Meas.Voltage: "+ str(self.__data[key]))
  
  def printDataDuringIrrad(self):
    ''' Print data
    '''
    print("Data points: "+str(len(self.__dataDuringIrrad)))
    for key in self.__dataDuringIrrad:
      print("Timestamp: " + str(key) + " Meas.Voltage: "+ str(self.__dataDuringIrrad[key]))

  def plotDataDuringIrrad(self, outputDir, splits = 1):
    ''' Plot data during irrad
    '''
    # Activate ATLAS style
    #plt.style.use('atlas')
    ampl.use_atlas_style()
    #print(plt.style.available)

    # Plot output name
    outputName = "ganfet"+str(self.__id)+"board"+str(self.__board)+"monitState"

    # Get data
    self.computeDataDuringIrrad()
    x, y, breaks, invalids = self.dataDuringIrradXY(True) # Time (x) given in hours
    # Convert data to Mrads
    x = [self.totalRad(i) for i in x]
    breaks = [(self.totalRad(i[0]), self.totalRad(i[1])) for i in breaks]
    invalids = [(self.totalRad(i[0]), self.totalRad(i[1])) for i in invalids]

    # Split data and data breaks in given number of segments
    xs = []
    ys = []
    breakLines = []
    invalidLines = []
    for i in range(splits):
      xs.append([])
      ys.append([])
      breakLines.append([])
      invalidLines.append([])

    # Get data per segment
    minTime = x[0]
    maxTime = x[-1]
    steps = (maxTime - minTime)/splits
    upperLims = [steps*(i+1) for i in range(splits)]
    for i in range(len(x)):
      for j in range(len(upperLims)):
        if x[i] < upperLims[j]:
          xs[j].append(x[i])
          ys[j].append(y[i])
          break

    # Get time break lines per segment
    for i in range(len(breaks)):
      breakLine1 = breaks[i][0]
      breakLine2 = breaks[i][1]
      for j in range(len(upperLims)):
        if breakLine1 < upperLims[j]:
          breakLines[j].append(breakLine1)
          break
      for j in range(len(upperLims)):
        if breakLine2 < upperLims[j]:
          breakLines[j].append(breakLine2)
          break

    # Get invalid data lines per segment
    for i in range(len(invalids)):
      invalidLine1 = invalids[i][0]
      invalidLine2 = invalids[i][1]
      for j in range(len(upperLims)):
        if invalidLine1 < upperLims[j]:
          invalidLines[j].append(invalidLine1)
          break
      for j in range(len(upperLims)):
        if invalidLine2 < upperLims[j]:
          invalidLines[j].append(invalidLine2)
          break

    # Plot
    fig, axs = plt.subplots(splits)
    if splits > 1:
      plt.subplots_adjust(left = 0.1, bottom=0.1, right=0.95, top=0.9, wspace=0.1, hspace=0.133*(splits-1))
    for i in range(splits):
      axs[i].plot(xs[i], ys[i], linewidth=0.5, color='black')
      if i == 0:
        minx = 0
      else:
        minx = upperLims[i-1]
      maxx = upperLims[i]
      axs[i].set_xlim([minx, maxx])
      axs[i].set_ylim([-3,15])
      for j in range(len(breakLines[i])):
        axs[i].axvline(x=breakLines[i][j], color='blue', linewidth = 1, linestyle='dashed')
      for j in range(len(invalidLines[i])):
        axs[i].axvline(x=invalidLines[i][j], color='green', linewidth = 1, linestyle='dashed')

      #secondaryAx = axs[i].secondary_xaxis('top', functions = (self.totalRad, self.totalIrradTime))
      #axs[i].tick_params(labelsize=6, pad=1, length=2)
      #secondaryAx.tick_params(labelsize=6, pad=1, length=2)
      if i == 0:
        #fullLength = maxx - minx
        axs[i].text(0,16,'ATLAS ITk', fontweight='bold', fontstyle='italic')
        axs[i].text(1.75,16,'Strip', fontstyle='italic')
        axs[i].text(6.9, 16, "Production batch sample GaNFET "+str(self.__id))#+ ", Board: "+str(self.__board), fontsize=8)
        axs[i].arrow(2.5, 12.5, 1.4, 0, width=0.08, color='blue', head_width=0.9, head_length=0.1)
        axs[i].arrow(3.3, 11.5, 0.6, 0, width=0.08, color='blue', head_width=0.9, head_length=0.1)
        axs[i].text(4.1, 10.75, 'Communication issue', color='blue')
      if i == 2:
        axs[i].arrow(34, 12.25, -1, 0, width=0.08,color='green', head_width=0.9, head_length=0.1)
        axs[i].text(31.3, 11.25, 'HV tripped', color='green')
        if self.__id == 9:
          axs[i].arrow(36.04, 5, -3.04, 0, width=0.08,color='red', head_width=0.9, head_length=0.1)
          axs[i].text(31.02, 4.25, 'Failure point', color='red')
        elif self.__id == 20:
          axs[i].arrow(32.84, 5, -0.5, 0, width=0.08,color='red', head_width=0.9, head_length=0.1)
          axs[i].text(30.35, 4.25, 'Failure point', color='red')
        elif self.__id == 60:
          axs[i].arrow(30.68, 5, 0.3, 0, width=0.08,color='red', head_width=0.9, head_length=0.1)
          axs[i].text(31.2, 4.25, 'Failure point', color='red')
      if i == 3:
        if self.__id == 70:
          axs[i].arrow(45.46, 5, 0.5, 0, width=0.08,color='red', head_width=0.9, head_length=0.1)
          axs[i].text(46.2, 4.25, 'Failure point', color='red')

      #axs[i].set_xlabel('Total ionizing dose [Mrad]')
      if splits % 2 == 1 and (i+1) == (splits+1)/2.0:
        axs[i].set_ylabel('Drain voltage divided by 50 [V]')
      elif splits % 2 == 0 and (i+1) == (splits/2.0):
        axs[i].set_ylabel('Drain voltage divided by 50 [V]')
        axs[i].yaxis.set_label_coords(-0.035, -(splits*0.1)/2.0)
      if i == splits-1:
        #axs[i].set_xlabel('Exposure time [hrs]')
        axs[i].set_xlabel('Total ionizing dose [Mrad]')

    try: 
     plt.savefig(os.path.join(outputDir, outputName+".png"), format='png', dpi=200)
     plt.savefig(os.path.join(outputDir, outputName+".pdf"), format='pdf', dpi=200)
     plt.savefig(os.path.join(outputDir, outputName+".eps"), format='eps', dpi=200)
    except:
     os.system("mkdir "+outputDir)
     plt.savefig(os.path.join(outputDir, outputName+".png"), format='png', dpi=200)
     plt.savefig(os.path.join(outputDir, outputName+".pdf"), format='pdf', dpi=200)
     plt.savefig(os.path.join(outputDir, outputName+".eps"), format='eps', dpi=200)
    plt.close()
