# GaNFET irradiation monitoring software

## General information
Software developed for python3 using: 
  * ```numpy```: For easy data handling
  * ```matplotlib```: For plotting
  * ```atlas_mpl_style```: For ATLAS plotting formatting

Software takes data from text files in the format shown in ```dataExample``` folder. In the case of BNL there are 4 boards.

**WARNING:** ```dataExample``` is only an example for format purposes. Do not add data in software folder to avoid accidental commits of large files.

## Run software
```
python3 controller.py
```
