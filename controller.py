import os
import time
import fileReader
import ganfet

if __name__=="__main__":
  # General variables
  dataDir = "/Users/felipegutierrez/Documents/CERN/ATLAS/work/ITk/strips/stave/module/powerboard/GaNFET/irradiation/2022gamma/data/" 
  # Collect data
  print("Collecting data...")
  startDataCollection = time.time()
  fileNames = os.listdir(dataDir)
  fileNames.sort() # To add the data in file order
  filePaths = [dataDir+fileName for fileName in fileNames]
  endDataCollection = time.time()
  print("-- " + str(endDataCollection-startDataCollection) + "s --")
  # Generate ganfet objects
  print("Generating ganfet objects...")
  startGanfetGen = time.time()
  ganfets = [ganfet.ganfet(i+1, i // 20 + 1) for i in range(80)] 
  endGanfetGen = time.time()
  print("-- " + str(endGanfetGen-startGanfetGen) + "s --")
  # Organize ganfet data
  print("Organizing ganfet data...")
  startGanfetDataOrg = time.time()
  for fp in filePaths:
    fileNameParts = fp.split("/")[-1].split(".")[0].split("_")
    if len(fileNameParts) != 2 or fileNameParts[1] not in ["B1", "B2", "B3", "B4"]:
      continue
    boardID = int(fileNameParts[1][1:])
    reader = fileReader.reader(fp)
    data = reader.data()
    for i in range(data.shape[1]):
      if i > 0:
        ganfetID = (i-1)+(boardID-1)*20+1 
        selectedData = data[['timestamp', 'ganfet'+str((ganfetID-1) % 20 + 1)]]
        #ganfets[(i-1)+(boardID-1)*20].addData(data, i)
        #print("GaNFET ID: "+ str(ganfetID))
        #print(selectedData)
        ganfets[ganfetID-1].addData(selectedData)
  endGanfetDataOrg = time.time()
  print("-- " + str(endGanfetDataOrg-startGanfetDataOrg) + "s --")
  # Plot monitoring data
  print("Plotting ganfet monitoring data...")
  startPlotting = time.time()
  for i in range(len(ganfets)):
    startGanfetPlotting = time.time()
    ganfets[i].plotDataDuringIrrad(os.path.join(dataDir,"plots"), 4)
    endGanfetPlotting = time.time()
    print("--Ganfet "+ str(i+1) + " plotting time: " + str(endGanfetPlotting-startGanfetPlotting))
  endPlotting = time.time()
  print("-- " + str(endPlotting-startPlotting) + "s --")
