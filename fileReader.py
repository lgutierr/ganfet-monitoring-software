import pandas as pd
import math
import datetime

class reader():
  def __init__(self, filePath):
    self.__filePath = filePath
    self.__dataFrame = None
    self.__read()

  def __read(self):
    ''' Reads file and outputs a dataFrame:
    '''
    df = pd.read_csv(self.__filePath, sep="\t", header=None)
    # Starting timestamp
    startTimeStamp = 0
    timestamps = []
    for index, row in df.iterrows():
      # Starting timestamp
      if index == 0:
        entryDate = [int(i) for i in row[0].split("/")]
        entryTimeStr = None
        entryTime = []
        pm = False
        if "PM" in row[1]:
          pm = True
          entryTimeStr = row[1].replace(" PM","")
        else:
          entryTimeStr = row[1].replace(" AM","")
        if pm:
          entryTimeTemp = entryTimeStr.split(":")
          if int(entryTimeTemp[0]) != 12:
            entryTime.append(int(entryTimeTemp[0])+12)
          else:
            entryTime.append(int(entryTimeTemp[0]))
          entryTime.append(int(entryTimeTemp[1]))
        else:
          entryTimeTemp = entryTimeStr.split(":")
          if int(entryTimeTemp[0]) == 12:
            entryTime.append(0)
          else:
            entryTime.append(int(entryTimeTemp[0]))
          entryTime.append(int(entryTimeStr[1]))
        startTimeStamp = datetime.datetime(year = entryDate[2], month = entryDate[0], day = entryDate[1], hour = entryTime[0], minute = entryTime[1] )
      # Get entry timestamp
      timeLapsed = row[2]*3600 # Convert time from hours to seconds
      seconds, decimals = math.modf(timeLapsed)
      milliseconds = 1000*decimals
      timeZoneAdaptation = 0#21600 # 6 hours more since I plot them from CERN
      entryTimestamp = startTimeStamp.timestamp() + timeLapsed + timeZoneAdaptation
      timestamps.append(entryTimestamp)
    # Create proper dataframe
    self.__dataFrame = pd.DataFrame(data={'timestamp': timestamps})
    for i in range(df.shape[1]):
      if i > 2:
        self.__dataFrame["ganfet"+str(i-2)] = df[i]

  def data(self):
    return self.__dataFrame
